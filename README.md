Notes from Dmitry:

I chose the following basic tasks:

- Use ScriptableObjects to configure gameplay variables
- Fix support for multiple aspect ratios
- Make UI resolution-independent
- Implement object pooling for shots

And this adanced task:

- Designers have also decided that they would like to be able to change game data on-the-fly, without deploying a new app. Provide them some way of being able to adjust these numbers dynamically.

For the advanced task I went with PlayerConnection and EditorConnection features. In order to try them in action, one needs to connect an Android device over USB and enable Development Build and Autoconnect Profiler. When the game is deployed, the developer needs to switch either profiler or console target from "Editor" to the connected Android device. From this point on, every time one of the configs in the "Configs" folder is changed in the editor, it serializes itself and sends itself to the player, which deserializes it and applies the new values to the config in question.

The task that deals with support for different aspect ratios also changes the width of the playable area, which will affect difficulty because density of emenies will be different on different aspect ratios. It could be solved by adjusting the spawning rate to the scale or by keeping the playable area the same size. In a real life scenario I would consult the game designer about this.

Aspect ratio also readjusts itself dynamically when screen resolution changes. If the game is only meant to be played in portrait mode on mobile, dynamic resolution detection can be disabled.

