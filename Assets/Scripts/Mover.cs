﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
	void Start ()
	{
        var movementConfig = GetComponent<IMovementConfigProvider>().GetMovementConfig();
		GetComponent<Rigidbody>().velocity = transform.forward * movementConfig.GetSpeed();
	}
}
