﻿using UnityEngine;

public interface IScalable
{
    void Scale(Vector2 scale);
}
