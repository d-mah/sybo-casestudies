﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleToFitWidth : MonoBehaviour, IScalable
{
    public virtual void Scale(Vector2 scale)
    {
        transform.localScale *= scale.x;
    }
}
