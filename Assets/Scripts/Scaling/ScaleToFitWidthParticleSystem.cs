﻿using UnityEngine;

public class ScaleToFitWidthParticleSystem : ScaleToFitWidth
{
    private ParticleSystem[] particleSystems;

    private void Awake()
    {
        particleSystems = GetComponentsInChildren<ParticleSystem>();
    }

    public override void Scale(Vector2 scale)
    {
        base.Scale(scale);
        for(var i = 0; i < particleSystems.Length; i++)
        {
            particleSystems[i].Stop();
            particleSystems[i].Play();
        }
    }
}
