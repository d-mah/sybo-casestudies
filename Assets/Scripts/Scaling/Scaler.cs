﻿using System;
using UnityEngine;
using System.Linq;

public class Scaler : MonoBehaviour
{
    public GameObject referenceObject;
    public Camera camera;

    private Vector2Int currentResolution; 

	void Update ()
    {
		if (Screen.width != currentResolution.x || Screen.height != currentResolution.y)
        {
            var bounds = GetCombinedRendererBounds(referenceObject);
            var viewPortRect = GetViewportRect(bounds);
            var scale = new Vector2(1 / viewPortRect.width, 1 / viewPortRect.height);
            NotifyScalables(scale);
            currentResolution.x = Screen.width;
            currentResolution.y = Screen.height;
        }
	}

    private void NotifyScalables(Vector2 scale)
    {
        var scalables = FindObjectsOfType<MonoBehaviour>().OfType<IScalable>();
        foreach (var scalable in scalables)
        {
            scalable.Scale(scale);
        }
    }

    private Bounds GetCombinedRendererBounds(GameObject obj)
    {
        var renderers = obj.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0)
        {
            throw new Exception("Cannot get bounds of a game object because it has no remderers");
        }

        var bounds = renderers[0].bounds;
        for (var i = 1; i < renderers.Length; i++)
        {
            bounds.Encapsulate(renderers[i].bounds);
        }

        return bounds;
    }

    private Rect GetViewportRect(Bounds bounds)
    {
        var min = bounds.min;
        var max = bounds.max;

        var corners = new Vector3[]
        {
            new Vector3(min.x, min.y, min.z),
            new Vector3(min.x, min.y, max.z),
            new Vector3(min.x, max.y, min.z),
            new Vector3(min.x, max.y, max.z),
            new Vector3(max.x, min.y, min.z),
            new Vector3(max.x, min.y, max.z),
            new Vector3(max.x, max.y, min.z),
            new Vector3(max.x, max.y, max.z),
        };

        for (var i = 0; i < corners.Length; i++)
        {
            corners[i] = camera.WorldToViewportPoint(corners[i]);
        }

        var minX = corners[0].x;
        var minY = corners[0].y;
        var maxX = corners[0].x;
        var maxY = corners[0].y;

        for (var i = 1; i < corners.Length; i++)
        {
            minX = Mathf.Min(minX, corners[i].x);
            minY = Mathf.Min(minY, corners[i].y);
            maxX = Mathf.Max(maxX, corners[i].x);
            maxY = Mathf.Max(maxY, corners[i].y);
        }

        return Rect.MinMaxRect(minX, minY, maxX, maxY);
    }
}
