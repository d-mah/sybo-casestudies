﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour
{
	public Transform shotSpawn;
    private IWeaponConfig config;
    private GameObjectPool pool;

	void Start ()
	{
        var provider = GetComponent<IWeaponConfigProvider>();
        config = provider.GetWeaponConfig();
        pool = PoolLocator.instance.Get(config.GetShot());
		InvokeRepeating ("Fire", config.GetDelay(), config.GetFireRate());
	}

	void Fire ()
	{
        var bolt = pool.GetInstance(shotSpawn.position, shotSpawn.rotation);
        GetComponent<AudioSource>().Play();
	}
}
