using UnityEngine;
using System.Collections;

public class EvasiveManeuver : MonoBehaviour
{
	private float currentSpeed;
	private float targetManeuver;

    private IEvasiveManeuverConfig config;

	void Start ()
	{
        var provider = GetComponent<IEvasiveManeuverConfigProvider>();
        config = provider.GetEvasiveManeuverConfig();

		currentSpeed = GetComponent<Rigidbody>().velocity.z;
		StartCoroutine(Evade());
	}
	
	IEnumerator Evade ()
	{
		yield return new WaitForSeconds (Random.Range (config.GetStartWait().x, config.GetStartWait().y));
		while (true)
		{
			targetManeuver = Random.Range (1, config.GetDodge()) * -Mathf.Sign (transform.position.x);
			yield return new WaitForSeconds (Random.Range (config.GetManeuverTime().x, config.GetManeuverTime().y));
			targetManeuver = 0;
			yield return new WaitForSeconds (Random.Range (config.GetManeuverWait().x, config.GetManeuverWait().y));
		}
	}
	
	void FixedUpdate ()
	{
		float newManeuver = Mathf.MoveTowards (GetComponent<Rigidbody>().velocity.x, targetManeuver, config.GetSmoothing() * Time.deltaTime);
		GetComponent<Rigidbody>().velocity = new Vector3 (newManeuver, 0.0f, currentSpeed);
		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp(GetComponent<Rigidbody>().position.x, config.GetBoundary().xMin, config.GetBoundary().xMax), 
			0.0f, 
			Mathf.Clamp(GetComponent<Rigidbody>().position.z, config.GetBoundary().zMin, config.GetBoundary().zMax)
		);
		
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0, 0, GetComponent<Rigidbody>().velocity.x * -config.GetTilt());
	}
}
