﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary 
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour, IScalable
{
	public Transform shotSpawn;
    public PlayerConfig config;

	private float nextFire;
    private GameObjectPool boltPool;
    private float xScaleFactor = 1;

    private void Start()
    {
        boltPool = PoolLocator.instance.Get(config.shot);
    }

    void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + config.fireRate;
            var newShot = boltPool.GetInstance(shotSpawn.position, shotSpawn.rotation);
			GetComponent<AudioSource>().Play ();
		}
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		GetComponent<Rigidbody>().velocity = movement * config.speed;
		
		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp (GetComponent<Rigidbody>().position.x, config.boundary.xMin * xScaleFactor, config.boundary.xMax * xScaleFactor), 
			0.0f, 
			Mathf.Clamp (GetComponent<Rigidbody>().position.z, config.boundary.zMin, config.boundary.zMax)
		);
		
		GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -config.tilt);
	}

    public void Scale(Vector2 scale)
    {
        xScaleFactor *= scale.x;
    }
}
