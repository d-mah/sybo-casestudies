﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponConfig
{
    GameObject GetShot();
    float GetFireRate();
    float GetDelay();
}
