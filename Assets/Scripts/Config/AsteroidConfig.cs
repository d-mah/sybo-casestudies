﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAsteroidConfig", menuName = "Asteroid Config", order = 52)]
public class AsteroidConfig : ConnectedScriptableObject, IMovementConfig, IRandomRotationConfig, IDestroyOnContactConfig
{
    public float speed;
    public float tumble;
    public int scoreValue;

    public int GetScoreValue()
    {
        return scoreValue;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public float GetTumble()
    {
        return tumble;
    }
}
