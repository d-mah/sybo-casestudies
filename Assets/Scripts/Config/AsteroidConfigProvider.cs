﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidConfigProvider : MonoBehaviour, IMovementConfigProvider, IRandomRotationConfigProvider, IDestroyOnContactConfigProvider
{
    public AsteroidConfig config;

    public IMovementConfig GetMovementConfig()
    {
        return config;
    }

    public IRandomRotationConfig GetRandomRotationConfig()
    {
        return config;
    }

    public IDestroyOnContactConfig GetDestroyOnContactConfig()
    {
        return config;
    }
}
