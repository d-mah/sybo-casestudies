﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRandomRotationConfig
{
    float GetTumble();
}
