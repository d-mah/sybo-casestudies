﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponConfigProvider
{
    IWeaponConfig GetWeaponConfig();
}
