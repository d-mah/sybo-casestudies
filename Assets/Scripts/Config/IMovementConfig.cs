﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovementConfig 
{
    float GetSpeed();
}
