﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEnemyShipConfig", menuName = "Enemy Ship Config", order = 54)]
public class EnemyShipConfig : ConnectedScriptableObject, IMovementConfig, IDestroyOnContactConfig, IWeaponConfig, IEvasiveManeuverConfig
{
    public float speed;
    public int scoreValue;

    public GameObject weaponShot;
    public float weaponFireRate;
    public float weaponDelay;

    public float maneuverTilt;
    public float maneuverDodge;
    public float maneuverSmoothing;
    public Boundary maneuverBoundary;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public Vector2 maneuverStartWait;
    

    public Boundary GetBoundary()
    {
        return maneuverBoundary;
    }

    public float GetDelay()
    {
        return weaponDelay;
    }

    public float GetDodge()
    {
        return maneuverDodge;
    }

    public float GetFireRate()
    {
        return weaponFireRate;
    }

    public Vector2 GetManeuverTime()
    {
        return maneuverTime;
    }

    public Vector2 GetManeuverWait()
    {
        return maneuverWait;
    }

    public int GetScoreValue()
    {
        return scoreValue;
    }

    public GameObject GetShot()
    {
        return weaponShot;
    }

    public float GetSmoothing()
    {
        return maneuverSmoothing;
    }

    public float GetSpeed()
    {
        return speed;
    }

    public Vector2 GetStartWait()
    {
        return maneuverWait;
    }

    public float GetTilt()
    {
        return maneuverTilt;
    }
}
