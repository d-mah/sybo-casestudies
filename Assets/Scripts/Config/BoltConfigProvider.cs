﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoltConfigProvider : MonoBehaviour, IMovementConfigProvider, IDestroyOnContactConfigProvider
{
    public BoltConfig config;

    public IDestroyOnContactConfig GetDestroyOnContactConfig()
    {
        return config;
    }

    public IMovementConfig GetMovementConfig()
    {
        return config;
    }
}
