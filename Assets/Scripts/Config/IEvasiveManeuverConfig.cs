﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEvasiveManeuverConfig
{
    Boundary GetBoundary();
    float GetTilt();
    float GetDodge();
    float GetSmoothing();
    Vector2 GetStartWait();
    Vector2 GetManeuverTime();
    Vector2 GetManeuverWait();
}
