﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipConfigProvider : MonoBehaviour, IMovementConfigProvider, IDestroyOnContactConfigProvider, IWeaponConfigProvider, IEvasiveManeuverConfigProvider
{
    public EnemyShipConfig config;

    public IDestroyOnContactConfig GetDestroyOnContactConfig()
    {
        return config;
    }

    public IEvasiveManeuverConfig GetEvasiveManeuverConfig()
    {
        return config;
    }

    public IMovementConfig GetMovementConfig()
    {
        return config;
    }

    public IWeaponConfig GetWeaponConfig()
    {
        return config;
    }
}
