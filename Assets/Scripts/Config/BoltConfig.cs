﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewBoltConfig", menuName = "Bolt Config", order = 53)]
public class BoltConfig : ConnectedScriptableObject, IMovementConfig, IDestroyOnContactConfig
{
    public float speed;

    public int GetScoreValue()
    {
        return 0;
    }

    public float GetSpeed()
    {
        return speed;
    }
}
