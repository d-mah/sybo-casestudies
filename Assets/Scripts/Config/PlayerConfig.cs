﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerConfig", menuName = "Player Config", order = 51)]
public class PlayerConfig : ConnectedScriptableObject
{
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public float fireRate;
}