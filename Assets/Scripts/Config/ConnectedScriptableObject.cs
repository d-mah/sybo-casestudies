﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.Networking.PlayerConnection;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Networking.PlayerConnection;
#endif

public class ConnectedScriptableObject : ScriptableObject
{
    [HideInInspector]
    [SerializeField]
    protected string id;

    private Guid idGuid;

    protected void OnEnable()
    {
#if UNITY_EDITOR
        if (string.IsNullOrEmpty(id))
        {
            idGuid = Guid.NewGuid();
            id = idGuid.ToString();
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssets();
        }
#endif

        if (idGuid == Guid.Empty)
        {
            idGuid = new Guid(id);
        }

        PlayerConnection.instance.Register(idGuid, OnMessageFromEditor);

#if UNITY_EDITOR
        EditorConnection.instance.Initialize();
        EditorConnection.instance.Register(idGuid, OnMessageFromPlayer);
#endif
    }

#if UNITY_EDITOR
    protected void OnValidate()
    {
        if (idGuid == Guid.Empty)
        {
            return;
        }

        var json = JsonUtility.ToJson(this);
        EditorConnection.instance.Send(idGuid, Encoding.UTF8.GetBytes(json));
    }
#endif

    private void OnMessageFromPlayer(MessageEventArgs args)
    {
        var text = Encoding.UTF8.GetString(args.data);
        Debug.Log("Message from player: " + text);
    }

    private void OnMessageFromEditor(MessageEventArgs args)
    {
        try
        {
            var json = Encoding.UTF8.GetString(args.data);
            JsonUtility.FromJsonOverwrite(json, this);
            PlayerConnection.instance.Send(idGuid, Encoding.UTF8.GetBytes("succesfully set values for instance: " + id));
        }
        catch (Exception e)
        {
            PlayerConnection.instance.Send(idGuid, Encoding.UTF8.GetBytes("error: " + e.Message));
        }
    }
}
