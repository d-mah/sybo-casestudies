﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
	public GameObject explosion;
	public GameObject playerExplosion;
	private GameController gameController;
    private IDestroyOnContactConfig config;
    private GameObjectPool explosionPool;
    private GameObjectPool playerExplosionPool;

	void Start ()
	{
        explosionPool = PoolLocator.instance.Get(explosion);
        playerExplosionPool = PoolLocator.instance.Get(playerExplosion);

        var configProvider = GetComponent<IDestroyOnContactConfigProvider>();
        config = configProvider.GetDestroyOnContactConfig();

		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Boundary" || other.tag == "Enemy")
		{
			return;
		}

		if (explosion != null)
		{
			explosionPool.GetInstance(transform.position, transform.rotation);
		}

		if (other.tag == "Player")
		{
			playerExplosionPool.GetInstance(other.transform.position, other.transform.rotation);
			gameController.GameOver();
		}

        gameController.AddScore(config.GetScoreValue());

        ReturnToPullOrDestroy(other.gameObject);
        ReturnToPullOrDestroy(gameObject);
	}

    void ReturnToPullOrDestroy(GameObject go)
    {
        var returnToPull = go.GetComponent<ReturnToPool>();
        if (returnToPull == null)
        {
            Destroy(go);
        }
        else
        {
            returnToPull.Return();
        }
    }
}