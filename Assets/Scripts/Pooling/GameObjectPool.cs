﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    public GameObject prefab;
    public int maxObjects = 20;

    private int prefabId;
    private Queue<GameObject> availableObjects = new Queue<GameObject>();

    private void Awake()
    {
        prefabId = prefab.GetInstanceID();
        PoolLocator.instance.Register(prefabId, this);
    }

    private void OnDestroy()
    {
        PoolLocator.instance.Unregister(prefabId);
    }

    public GameObject GetInstance(Vector3 position, Quaternion rotation)
    {
        var instance = GetInstance();
        instance.transform.SetPositionAndRotation(position, rotation);
        return instance;
    }

    public GameObject GetInstance()
    {
        GameObject newInstance;
        if (availableObjects.Count > 0)
        {
            newInstance = availableObjects.Dequeue();
            newInstance.SetActive(true);
        }
        else
        {
            newInstance = Instantiate(prefab);
            var returnToPool = newInstance.GetComponent<ReturnToPool>();
            if (returnToPool != null)
            {
                returnToPool.pool = this;
            }
        }

        return newInstance;
    }

    public void ReturnInstance(GameObject instance)
    {
        if (availableObjects.Count >= maxObjects)
        {
            Destroy(instance);
            return;
        }

        instance.SetActive(false);
        availableObjects.Enqueue(instance);
    }
}
