﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToPoolIfNotRendered : ReturnToPool
{
    private Renderer[] renderers;

    private void Awake()
    {
        renderers = GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0)
        {
            throw new Exception("ReturnToPoolIfNotRendered requires at least one renderer");
        }
    }

    private void Update()
    {
        var visible = false;
        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i].isVisible)
            {
                visible = true;
                break;
            }
        }

        if (!visible)
        {
            Return();
        }
    }
}
