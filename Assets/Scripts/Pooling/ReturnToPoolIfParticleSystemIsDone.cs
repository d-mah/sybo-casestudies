﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToPoolIfParticleSystemIsDone : ReturnToPool {

    private ParticleSystem[] particleSystems;

    private void Awake()
    {
        particleSystems = GetComponentsInChildren<ParticleSystem>();
        if (particleSystems.Length == 0)
        {
            throw new Exception("ReturnToPoolIfParticleSystemIsDone requires at least one particle system");
        }
    }

    private void Update()
    {
        var done = true;
        for (int i = 0; i < particleSystems.Length; i++)
        {
            if (particleSystems[i].isEmitting)
            {
                done = false;
                break;
            }
        }

        if (done)
        {
            Return();
        }
    }
}
