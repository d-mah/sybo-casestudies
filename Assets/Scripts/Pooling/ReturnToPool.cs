﻿using System;
using UnityEngine;

public class ReturnToPool : MonoBehaviour
{
    public GameObjectPool pool;

    public void Return()
    {
        pool.ReturnInstance(gameObject);
    }
}
