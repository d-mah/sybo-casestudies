﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolLocator
{
    private static PoolLocator _instance;

    public static PoolLocator instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PoolLocator();
            }

            return _instance;
        }
    }

    private Dictionary<int, GameObjectPool> pools = new Dictionary<int, GameObjectPool>();

    public void Register(int objectId, GameObjectPool pool)
    {
        if (!pools.ContainsKey(objectId))
        {
            pools.Add(objectId, pool);
        }
    }

    public void Unregister(int objectId)
    {
        pools.Remove(objectId);
    }

    public GameObjectPool Get(GameObject prefab)
    {
        if (prefab == null)
        {
            return null;
        }

        return Get(prefab.GetInstanceID());
    }

    public GameObjectPool Get(int objectId)
    {
        if (!pools.ContainsKey(objectId))
        {
            return null;
        }

        return pools[objectId];
    }
}
