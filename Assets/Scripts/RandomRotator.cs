﻿using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour 
{
	void Start ()
	{
        var provider = GetComponent<IRandomRotationConfigProvider>();
        var config = provider.GetRandomRotationConfig();
		GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * config.GetTumble();
	}
}