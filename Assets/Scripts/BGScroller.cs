﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour, IScalable
{
	public float scrollSpeed;
	public float tileSizeZ;

    private float scaleFactor = 1;

	private Vector3 startPosition;

    public void Scale(Vector2 scale)
    {
        scaleFactor *= scale.x;
    }

    void Start ()
	{
		startPosition = transform.position;
	}

	void Update ()
	{
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ * scaleFactor);
		transform.position = startPosition + Vector3.forward * newPosition;
	}
}